<?php

use App\Entity\Buyer;
use App\Entity\Order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $buyer = Buyer::all();
        $orders = $buyer->map(
            function (Buyer $buyer) {
                return factory(Order::class, 2)->make([
                    'buyer' => $buyer->id
                ]);
            }
        );
        DB::table('order')->insert($orders->flatten()->toArray());
    }
}