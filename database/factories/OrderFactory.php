<?php
/* @var $factory \Illuminate\Database\Eloquent\Factory */
use \App\Entity\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'date' => $faker->dateTime($max = 'now'),
        'buyer' =>  \App\Entity\Buyer::query()->inRandomOrder()->first()->id,
    ];
});
