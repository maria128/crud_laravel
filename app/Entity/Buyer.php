<?php


namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Buyer  extends Model
{

    protected $table = 'buyer';

    protected $fillable = [
        'name',
        'surname',
        'country',
        'city',
        'addressLine',
        'phone',
    ];

    public function order()
    {
        return $this->hasMany(Order::class, 'buyer', 'id');
    }

    public function getCreatedAt(): Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }

    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function buyerAddress()
    {
        return $this->country . ' ' . $this->city . ' ' . $this->addressLine;
    }
}